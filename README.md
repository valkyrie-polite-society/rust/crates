# Crates

A listing of crates I believe are good for use and/or contribution

## HashiCorp Stack

### Consul
*rs-consul*
- Async support (compared to sync only for consul)
- 40K+ downloads (compared to 9600 for consul)
- 32K+ recent downloads (compared to 700 for consul)
- Updated 4 months ago

### Vault
*hashicorp_vault*
- A bit limited but seems like the most used

### Google

*google-storage1* (or any Mako-generated API client)
